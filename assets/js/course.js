//Scenario: when accessing a course, the url is as follows: course.html?courseId=5e1645e8214
// in order to get the actual details, we need to get the value after the ?courseId=

//window.location.search -> returns the query string part of the URL
// console.log(window.location.search) //?courseId=5ecfbe52dbef30f246c9e288
let adminUser = localStorage.getItem("isAdmin")
//to get the actual courseId, we need to use URL search params to access specific parts of the query
let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")

// console.log(courseId)
//retrieve the JWT stored in localStorage
let token = localStorage.getItem('token')

let dropOption = localStorage.getItem('hasDropOption')
let isLogged = localStorage.getItem("isLogged")

if(isLogged && adminUser == "false"){
	document.getElementById('nav-login').innerHTML = null
	document.getElementById('nav-register').innerHTML = null
} else if(isLogged && adminUser){
	document.getElementById('nav-login').innerHTML = null
	document.getElementById('nav-register').innerHTML = null
	document.getElementById('nav-profile').innerHTML = null
} else {
	document.getElementById('nav-profile').innerHTML = null
	document.getElementById('nav-logout').innerHTML = null
}

//populate the information of a course using fetch
let courseName = document.getElementById('courseName')
let courseDesc = document.getElementById('courseDesc')
let coursePrice = document.getElementById('coursePrice')
let enrollContainer = document.getElementById('enrollContainer')
let enrollContainer2 = document.getElementById('enrollContainer2')
let enrolleesHeader = document.getElementById('enrolleesHeader')
let enrolleesList = document.getElementById('enrolleesList')


if(adminUser !== "true" && isLogged ){
fetch(`https://afternoon-wildwood-58236.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
		if(dropOption == "true"){
		//add drop for !adminUser
			enrollContainer.innerHTML = `<a href="./profile.html" class="btn btn-block btn-info">Back to Profile</a>`
			enrollContainer2.innerHTML = `
											<button id="dropButton" class="btn btn-danger btn-block">Drop</button>
									`
			document.querySelector('a.btn').addEventListener('click', () => {
				localStorage.setItem('hasDropOption', false)
			})
			document.getElementById('dropButton').addEventListener('click', () => {

				let isSure = confirm(`Are you sure you want to drop this course?`)
				if(isSure){
					fetch(`https://afternoon-wildwood-58236.herokuapp.com/api/users/${courseId}/drop`, {
						method : 'DELETE',
						headers : {
							"Content-Type" : "application/json",
							"Authorization" : `Bearer ${token}`
						},
						body : JSON.stringify({
							courseId : courseId
						})
					})
					.then(res => {
						return res.json()
					})
					.then(data => {
						if(data){
							alert('You have dropped the course')
							// localStorage.setItem("hasDropOption", null)
							localStorage.setItem("hasDropOption", false)
							window.location.replace(`./profile.html`)
						} else {
							alert(`Something went wrong`)
						}
					})
				} else {
					alert(`OK`)
				}
			})
		} else {
				courseName.innerHTML = data.name
				courseDesc.innerHTML = data.description
				coursePrice.innerHTML = data.price
				enrollContainer.innerHTML = `
											<button id="enrollButton" class="btn btn-primary btn-block">Enroll</button>
										`
				enrollContainer2.innerHTML= `<a href="./courses.html" class="btn btn-block btn-info">Back to Courses</a>
				`
				document.querySelector('#enrollButton').addEventListener('click', () => {
				//no need to .preventDefault as the button specified doesn't have any default actions when clicked
				//fetch the enroll function of the backend and send the necessary data to the backend
				//token and courseId
				// url : http://localhost:3000/api/users/enroll
				//Mini exercise : prepare the fetch statement to the backend
				//for the 2 .then's, just console.log the data
				fetch('https://afternoon-wildwood-58236.herokuapp.com/api/users/enroll', {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json',
						'Authorization' : `Bearer ${token}`
					},
					body : JSON.stringify({
						courseId : courseId
					})
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					if(data){
						alert('You have enrolled successfully')
						window.location.replace('./courses.html')
					} else {
						alert('Enrollment failed')
					}
				})
				})
		}
  })
} else {
	fetch(`https://afternoon-wildwood-58236.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {
		let enrollees
		courseName.innerHTML = data.name
		courseDesc.innerHTML = data.description
		coursePrice.innerHTML = data.price
		if(data.enrollees < 1){
			enrollContainer.innerHTML = `There are currently no enrollees to this course.
									<br> <a href="./courses.html" class="btn btn-primary btn-block"> Back to Courses </a>
					`
			enrolleesHeader.innerHTML = null
		} else {
			data.enrollees.map(enrollee => {
				fetch(`https://afternoon-wildwood-58236.herokuapp.com/api/users/${enrollee.userId}`)
				.then(res => res.json())
				.then(data => {
					let lastname = data.lastname
					let firstname = data.firstname
					firstname = firstname == undefined ? data.firstName : firstname
					lastname = lastname == undefined ? data.lastName : lastname
					return (`
								<li>${lastname.charAt(0).toUpperCase() + lastname.slice(1)}, ${firstname.charAt(0).toUpperCase() + firstname.slice(1)}</li>
					`)			
				})
				.then(data => {
					enrolleesList.innerHTML += data
				})
			}).join("")
			enrollContainer.innerHTML = `<a href="./courses.html" class="btn btn-block btn-info">Back to Courses</a>`
			enrolleesHeader.innerHTML = `Enrollees:`
		}
})
}

