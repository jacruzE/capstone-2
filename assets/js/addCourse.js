let formSubmit = document.getElementById('createCourse')

formSubmit.addEventListener('submit', e => {
	e.preventDefault()

	let courseName = document.getElementById('courseName').value
	let description = document.querySelector('#courseDescription').value
	let price = document.getElementById('coursePrice').value

	//get the JWT from localStorage
	let token = localStorage.getItem('token')

	//use fetch to send the data for the new course to the backend
	fetch('https://afternoon-wildwood-58236.herokuapp.com/api/courses', {
		method : "POST",
		headers : {
			'Content-Type' : 'application/json',
			//add the bearer token to satisfy auth.verify
			'Authorization' : `Bearer ${token}` // the bearer token similar to POSTMAN
		},
		body : JSON.stringify({
			name : courseName,
			description : description,
			price : price
		})
	})
	.then(res => {
		return res.json()
	})
	.then(data => {
		if(data) {
			window.location.replace('./courses.html')
		} else {
			alert('Course not added')
		}
	})
})