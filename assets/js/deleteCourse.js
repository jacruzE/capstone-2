let courseId = new URLSearchParams(window.location.search).get('courseId')

let token = localStorage.getItem('token')

let youSure = confirm(`Are you sure you want to proceed?`)

function getCourseName(x){
	courseName = x.name
}

fetch(`https://afternoon-wildwood-58236.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	getCourseName(data)
})
if(youSure){
fetch(`https://afternoon-wildwood-58236.herokuapp.com/api/courses/${courseId}`, {
	method: 'DELETE',
	headers: {
		'Authorization' : `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	if(data){
		document.getElementById('deleteStatusBox').innerHTML = `${courseName} successfully archived. <br><br> <h5 class="text-muted">Course ID: ${courseId}</h5>`
	} else {
		alert(`We have encountered a problem`)
		// window.location.replace(`./courses.html`)
	}
})
} else {
	document.getElementById('deleteStatusBox').innerHTML = `This course will remain active!`
}


//Exercise ↑↑↑
//use the lesson for the enroll to archive the course

//Steps:

//1.get the id from the url: 
//	to get the actual courseId, we need to use URL search params to access specific parameters
//		let courseId = new URLSearchParams(window.location.search).get('courseId')
// OR : let params = new URLSearchParams(window.location.search)
//		let courseId = params.get('courseId')
// 		token = localStorage.getItem('token')


//2. use the id as a parameter to the fetch and specify the url
//	fetch(`http://localhost:3000/api/courses/${courseId}`, {
// 	method : 'DELETE',
// 	headers : {
// 		'Authorization' : `Bearer ${token}`
// 	}
// })
// .then(res => {return res.json()})
// .then(data => {
// 	if(data){
// 		alert('Course archived')
// 	} else {
// 		alert('Error')
// 	}
// })


//3.