let registerForm = document.querySelector("#registerUser")

registerForm.addEventListener("submit", e => {
	e.preventDefault()


	//get values from the form
	let firstName = document.getElementById("firstName").value
	let lastName = document.getElementById("lastName").value
	let mobileNo = document.getElementById("mobileNumber").value
	let email = document.getElementById("userEmail").value
	let password1 = document.getElementById("password1").value
	let password2 = document.getElementById("password2").value

	//validate user inputs
	if((password1 !== "" && password2 !== "") && (password1 === password2) && (mobileNo.length == 11)){

		//check if the database already has the email
		//fetch() - a built-in js function that allows getting of data from another source without the need to refresh a page
		//fetch send data to the URL provided with the following parameters
		//method -> HTTP method
		//header -> what kind of data to send
		//body -> content of the req.body
		fetch("https://afternoon-wildwood-58236.herokuapp.com/api/users/email-exists", {
			method : "POST",
			headers : {
				"Content-Type" : "application/json"
			},
			body : JSON.stringify({
				email : email
			})
		})
		.then(response => response.json())
		//converted response to json
		.then(data => {
		//response.json assigned to data
			//if data == false, no duplicate exists
			if(data === false){
				//fetch registration route
				fetch("https://afternoon-wildwood-58236.herokuapp.com/api/users", {
					method : "POST",
					headers : {
						"Content-Type" : "application/json"
					},
					body : JSON.stringify({
						firstname : firstName,
						lastname : lastName,
						email : email,
						password : password1,
						mobileNo : mobileNo
					})
				})
				.then(response => response.json())
				.then(data => {
					//if data is true, registration is successful
					if (data) {
						alert("Registration successful")
					} else {
						alert("Registration went wrong")
					}
				})
			} else {
				alert("Email already exists")
			}
		})
	} else {
		alert("Invalid inputs")
	}
})