let loginForm = document.getElementById("logInUser")

loginForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let email = document.getElementById("userEmail").value
	let password = document.getElementById("password").value

	// console.log(email)
	// console.log(password)

	if(email == "" || password == ""){
		alert("Please input your email and/or password")
	} else {
		fetch("https://afternoon-wildwood-58236.herokuapp.com/api/users/login", {
			method : "POST",
			headers : {
				"Content-Type" : "application/json"
			},
			body : JSON.stringify({
				email : email,
				password : password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data !== false && data.access !== null){
				localStorage.setItem('token', data.access)
				//goes to browser's localStorage = {token : data.access}

				fetch('https://afternoon-wildwood-58236.herokuapp.com/api/users/details', {
					headers: {
						'Authorization' : `Bearer ${data.access}`
						}
					})
					.then(res => res.json())
					.then(data => {
						localStorage.setItem("id", data._id)
						localStorage.setItem("isAdmin", data.isAdmin)
						localStorage.setItem("isLogged", true)
						//redirect to courses page
						if(data.isAdmin){
							window.location.replace("./courses.html")
						} else {
							window.location.replace("./profile.html")
						}
					})
			} else {
				alert('something went wrong')
			}
		})
	}
})