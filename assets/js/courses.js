let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector("#adminButton")
let cardFooter; //use this to add a button at the bottom of each card to go to a specific course
let isLogged = localStorage.getItem("isLogged")
let addCourseOption = localStorage.getItem('addCourseOption')
let container = document.querySelector("#coursesContainer")

if(isLogged && adminUser == "false"){
	document.getElementById('nav-login').innerHTML = null
	document.getElementById('nav-register').innerHTML = null
} else if(isLogged && adminUser){
	document.getElementById('nav-login').innerHTML = null
	document.getElementById('nav-register').innerHTML = null
	document.getElementById('nav-profile').innerHTML = null
} else {
	document.getElementById('nav-profile').innerHTML = null
	document.getElementById('nav-logout').innerHTML = null
}


if (adminUser == "false" || !adminUser) {
	modalButton.innerHTML = null;
} else {
	modalButton.innerHTML = 
	`
	<div class="col-md-2 offset-md-10">
	<a href="./addCourse.html" class="btn btn-block btn-primary">
	Add Course
	</a>
	</div>
	`
}



if(adminUser != "true" || adminUser == null){
	fetch('https://afternoon-wildwood-58236.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {
		let courseData;
		if(data.length < 1){
			courseData = "No courses available"
		} else {
courseData = data.map(course => { //break the array into component elements 
//console.log(course._id)
cardFooter = `<a href="./course.html?courseId=${course._id}" value=${course._id} class = "btn btn-primary text-white btn-block">Go to Course</a>`
let cardFooter2 = `
<a href="./course.html?courseId=${course._id}" value=${course._id} class = "btn btn-primary text-white btn-block"> Get </a>
`
if(isLogged == "true" || isLogged != null){
	return (`
		<div class="col-md-6 my-3">
		<div class="card">
		<div class="card-body">
		<h5 class="card-title">${course.name}</h5>
		<p class="card-text text-left">${course.description}</p>
		<p class="card-text text-right">&#8369; ${course.price}</p>
		</div>
		<div class="card-footer">
		${cardFooter2}
		</div>
		</div>
		</div>
		`)
} else {
	return (`
		<div class="col-md-6 my-3">
		<div class="card">
		<div class="card-body">
		<h5 class="card-title">${course.name}</h5>
		<p class="card-text text-left">${course.description}</p>
		<p class="card-text text-right">&#8369; ${course.price}</p>
		</div>
		<div class="card-footer">
		${cardFooter}
		</div>
		</div>
		</div>
		`)
}
}).join("")
}
container.innerHTML = courseData
if(addCourseOption == "true"){
	document.querySelectorAll('#coursesContainer .addCourse').forEach(button => {
		button.addEventListener('click', () => {
			fetch('https://afternoon-wildwood-58236.herokuapp.com/api/users/enroll', {
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json',
					'Authorization' : `Bearer ${token}`
				},
				body : JSON.stringify({
					courseId : courseId
				})
			})
			.then(res => {
				return res.json()
			})
			.then(data => {
				if(data){
					alert('You have enrolled successfully')
					window.location.replace('./courses.html')
				} else {
					alert('Enrollment failed')
				}
			})
		})
	})
}
})
} else if (adminUser =="true"){
	fetch(`https://afternoon-wildwood-58236.herokuapp.com/api/courses/all`)
	.then(res => res.json())
	.then(data => {
		let courseData;
		if(data.length < 1){
			courseData = "No courses available"
		} else {
			courseData = data.map(course => {
				let cardFooter2 = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-info text-white btn-block">View Details</a>`
				let status = course.isActive ? 'Active' : 'Archived'
				let statusColor = course.isActive ? 'success' : 'warning'
				cardFooter = (status=='Active') ? `<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class = "btn btn-danger text-white btn-block"> Archive </a>` : `<a href="./unarchiveCourse.html?courseId=${course._id}" value=${course._id} class = "btn btn-warning text-white btn-block"> Unarchive </a>`

				return (`
					<div class="col-md-6 my-3">
					<div class="card">
					<div class="card-header pb-0 text-${statusColor}">
					${status}
					</div>
					<div class="card-body">
					<h5 class="card-title">${course.name}</h5>
					<p class="card-text text-left">${course.description}</p>
					<p class="card-text text-right">Enrollees :  ${course.enrollees.length}</p>
					</div>
					<div class="card-footer">
					${cardFooter2}
					</div>
					<div class="card-footer">
					${cardFooter}
					</div>
					</div>
					</div>
					`)
			}).join("")
		}
		let container = document.querySelector("#coursesContainer")

		container.innerHTML = courseData
	})
} else {

}