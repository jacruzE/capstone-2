let courseId = new URLSearchParams(window.location.search).get('courseId')

let token = localStorage.getItem('token')

let youSure = confirm(`Are you sure you want to proceed?`)

function getCourseName(x){
	courseName = x.name
}

fetch(`https://afternoon-wildwood-58236.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	getCourseName(data)
})

if(youSure){
fetch(`https://afternoon-wildwood-58236.herokuapp.com/api/courses/archive/${courseId}`, {
	method: 'DELETE',
	headers: {
		'Authorization' : `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	if(data){
		document.getElementById('deleteStatusBox').innerHTML = `${courseName.toUpperCase()} successfully restored. <br><br> <h5 class="text-muted">Course ID: ${courseId}</h5>`
	} else {
		alert(`We have encountered a problem`)
		// window.location.replace(`./courses.html`)
	}
})
} else {
	document.getElementById('deleteStatusBox').innerHTML = `This course will remain archived!`
}
