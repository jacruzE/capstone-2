let adminUser = localStorage.getItem('isAdmin')
let isLogged = localStorage.getItem("isLogged")


if(isLogged && adminUser == "false"){
	document.getElementById('nav-login').innerHTML = null
	document.getElementById('nav-register').innerHTML = null
} else if(isLogged && adminUser){
	document.getElementById('nav-login').innerHTML = null
	document.getElementById('nav-register').innerHTML = null
	document.getElementById('nav-profile').innerHTML = null
} else {
	document.getElementById('nav-profile').innerHTML = null
	document.getElementById('nav-logout').innerHTML = null
}