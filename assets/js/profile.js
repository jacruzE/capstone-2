let welcome = document.getElementById('welcome')
let profileSec = document.getElementById('profileSection')
let coursesSec = document.getElementById('coursesSection')

let token = localStorage.getItem('token')
let isLogged = localStorage.getItem("isLogged")

fetch(`https://afternoon-wildwood-58236.herokuapp.com/api/users/details`, {
	headers: {
		"Authorization" : `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	let mobile = data.mobileNo.split("")
	welcome.innerHTML = `Welcome, ${data.firstname}!`
	profileSec.innerHTML = `
							<div class="text-center card-header"><h4>Profile</h4></div>
							<li class="list-group-item">name:<strong><em class="offset-1">${data.firstname} ${data.lastname}</em></strong></li>
							<li class="list-group-item">email:<strong><em class="offset-1">${data.email}</em></strong></li>
							<li class="list-group-item">mobile:<strong><em class="offset-1">${mobile[0]+mobile[1]+mobile[2]+mobile[3]}-${mobile[4]+mobile[5]+mobile[6]}-${mobile[7]+mobile[8]+mobile[9]+mobile[10]}</em></strong></li>
						`
	return data.enrollments
})
.then(res => {
	res.map(course => {
		fetch(`https://afternoon-wildwood-58236.herokuapp.com/api/courses/${course.courseId}`)
	.then(res => res.json())
	.then(data =>{
		if(data !== null && data !==undefined){
			return r = { "card" :(`
					<a href="./course.html?courseId=${data._id}" value=${data._id} class="btn btn-outline-light btn-block text-dark text-left dropOption">
						<li class="list-group-item">
							<strong>${data.name}</strong>
							<p>${data.description}</p>
						</li>
					</a>
				`), "instance" : 1}
		} else {
			return null
		}
	})
	.then(data => {
		coursesSec.innerHTML += data.card
		document.querySelectorAll("#coursesSection a.dropOption").forEach(card =>{
			card.addEventListener("click", (e) => {
			localStorage.setItem("hasDropOption", true)
		})	
		})
	})
	}).join("")

	if(res.length < 1){
		coursesSec.innerHTML = `
								<div class="text-center card-header d-md-flex">
									<div class="mx-auto">
										<span class="h4">Enrolled courses:</span>
									</div> 
									<div class="mr-1"><a href="./courses.html" class="btn btn-primary" id="addCourseButton"><i class="fas fa-plus"></i> Course</a>
									</div>
								</div>
								<h5 class="text-center text-muted my-5">You are not enrolled to any course right now</h5>
								`
	} else {
		coursesSec.innerHTML = `
						<div class="text-center card-header d-md-flex">
							<div class="mx-auto">
								<span class="h4">Enrolled courses:</span>
							</div> 
							<div class="mr-1">
								<a href="./courses.html" class="btn btn-primary" id="addCourseButton">
									<i class="fas fa-plus"></i> Course
								</a>
							</div>
						</div>

						`
	}
})

